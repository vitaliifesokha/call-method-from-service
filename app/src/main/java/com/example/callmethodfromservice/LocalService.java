package com.example.callmethodfromservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class LocalService extends Service {
    private IBinder binder;

    @Override
    public void onCreate() {
        super.onCreate();
        binder = new LocalBinder();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    class LocalBinder extends Binder {
        LocalService getService() {
            return LocalService.this;
        }
    }

    public void foo(){
        //Todo
    }
}
